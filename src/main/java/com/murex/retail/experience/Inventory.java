package com.murex.retail.experience;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Splitter.on;

public final class Inventory {
  static final String NOT_AVAILABLE_ITEM_TEXT = "N/A";

  private static final String FILE_LOCATION = "src/main/resources/Inventory.csv";
  private static final Path PATH = Paths.get(FILE_LOCATION);
  private static final Logger LOG = LogManager.getLogger(Inventory.class);
  private static final String ITEM_FIELD_SEPARATOR = "\t|\t";
  private static final String ITEM_FIELD_DEFAULT_DELIMITER = ",";

  /**
   * Logs out all the items' data. The item's data are listed in the following order: ID, Category, Name, Brand, Product Line
   * ,Number Of Cores, Processor Clock Speed, Graphic Clock Speed, Dimension
   * ,Resolution, Color, Interface, Size, Price and Quantity
   *
   * @throws IOException
   */
  public static void printAllItems() throws IOException {
    Stream<String> lines = Files.lines(PATH).parallel().unordered();
    List<String> reformattedLines = lines
            .map(line -> StringUtils.replace(line, ITEM_FIELD_DEFAULT_DELIMITER, ITEM_FIELD_SEPARATOR))
            .collect(Collectors.toUnmodifiableList());

    reformattedLines.forEach(LOG::info);
  }

  /**
   * Returns the data of an item as an iterable. The item's data are listed in the following order: ID, Category, Name, Brand, Product Line
   * ,Number Of Cores, Processor Clock Speed, Graphic Clock Speed, Dimension
   * ,Resolution, Color, Interface, Size, Price and Quantity
   *
   * @param id The item's ID
   * @return An Iterable having the item's fields
   * @throws IOException
   */
  public static Iterable<String> getItemFields(@Nonnull String id) throws IOException {
    Stream<String> lines = Files.lines(PATH).parallel();

    return lines.map((line) -> on(ITEM_FIELD_DEFAULT_DELIMITER).trimResults().split(line))
            .filter((itemFields) -> {
              //Check if we reached the item
              Iterator<String> fields = itemFields.iterator();

              if (fields.hasNext()) {
                return id.equals(fields.next());
              }
              return false;
            })
            .findFirst().orElse(null);
  }


  /**
   * Returns the data of an item as a String. The item's data are listed in the following order: ID, Category, Name, Brand, Product Line
   * ,Number Of Cores, Processor Clock Speed, Graphic Clock Speed, Dimension
   * ,Resolution, Color, Interface, Size, Price and Quantity
   *
   * @param id the item's ID
   * @return The item's data as a String
   * @throws IOException
   */
  public static String getItemRawData(@Nonnull String id) throws IOException {
    Stream<String> lines = Files.lines(PATH).parallel();

    return lines
            .filter(line -> StringUtils.startsWith(line, id))
            .map(line -> StringUtils.replace(line, ITEM_FIELD_DEFAULT_DELIMITER, ITEM_FIELD_SEPARATOR))
            .findAny().orElse(NOT_AVAILABLE_ITEM_TEXT);
  }

  /**
   * Returns a specific field from the designated Item as a String. The item's fields are as follows in the following order: Category, Name, Brand, Product Line
   * ,Number Of Cores, Processor Clock Speed, Graphic Clock Speed, Dimension
   * ,Resolution, Color, Interface, Size, Price and Quantity
   *
   * @param id the item's ID
   * @return The item's field data as a String
   * @throws IOException
   */
  public static String getItemField(@Nonnull String id, int itemIndex) throws IOException {
    Stream<String> lines = Files.lines(PATH).parallel();
    Iterator<String> itemFields = lines.map((line) -> on(ITEM_FIELD_DEFAULT_DELIMITER).trimResults().split(line).iterator())
            .filter((fields) -> {
              //Check if we reached the item (1st item field is the ID)
              if (fields.hasNext()) {
                return id.equals(fields.next());
              }
              return false;
            })
            .findAny().orElse(null);

    //move through item fields till you get to the intended field
    while (ObjectUtils.allNotNull(itemFields) && itemIndex >= 0 && Objects.requireNonNull(itemFields).hasNext()) {
      if (itemIndex == 0)
        return itemFields.next().trim();

      itemIndex--;
      itemFields.next();
    }
    return NOT_AVAILABLE_ITEM_TEXT;
  }
}