package com.murex.retail.experience;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import static com.murex.retail.experience.Inventory.NOT_AVAILABLE_ITEM_TEXT;


class InventoryTest {

  @Test
  void printAllItemsWorksCorrectly() {
    PrintStream old = System.out;

    Assertions.assertDoesNotThrow(() -> {
      String result = "";

      try (ByteArrayOutputStream loggedDataStream = new ByteArrayOutputStream();
           PrintStream ps = new PrintStream(loggedDataStream)) {

        System.setOut(ps);

        Inventory.printAllItems();
        result = loggedDataStream.toString();
      } catch (IOException e) {
        throw new IOException("Couldn't read log");
      }

      //Revert changes
      System.out.flush();
      System.setOut(old);

      Assertions.assertTrue(result.contains("[main] INFO  com.murex.retail.experience.Main - 4aded436-c6db-4036-9537-43979f69875a\t|\tCPU     \t|\tIntel Core ii7-8705G                       \t|\tIntel          \t|\tCore i7                    \t|\t4              \t|\t4.10 GHz             \t|\t1.20 GHz           \t|\tN/A                   \t|\tN/A        \t|\tN/A          \t|\tN/A      \t|\tN/A  \t|\t50   \t|\t25")
              , "Something seems to be wrong with the Log Output");
    }, "Couldn't read log");
  }

  @Test
  void getItemIteratorRetrievesItemCorrectly() throws IOException {
    String itemID = "b1ea96b6-8633-4025-aecd-edbe10769d82";
    final Iterable<String> result = Inventory.getItemFields(itemID);

    final List<String> expectedData = List.of(itemID
            , "CPU", "Atom Processor C3850", "Intel"
            , "Atom", "12", "2.10 GHz", "N/A", "N/A", "N/A"
            , "N/A", "N/A", "N/A", "75", "30");

    Assertions.assertIterableEquals(expectedData, result);
  }

  @Test
  void getItemIteratorReturnNullForUnavailableItem() throws IOException {
    String fakeItemID = "fakeId88-8633-4025-aecd-edbe10769d82";
    final Iterable<String> result = Inventory.getItemFields(fakeItemID);

    Assertions.assertNull(result);
  }

  @Test
  void getItemRetrievesCorrectItem() throws IOException {
    String itemID = "97222f34-2e84-48a3-a165-97962cdc8c95";
    final String result = Inventory.getItemRawData(itemID);

    String rawExpectedOutput = "97222f34-2e84-48a3-a165-97962cdc8c95\t|\tMonitor \t|\tAsus VP229HA                               \t|\tAsus           \t|\tN/A                        \t|\tN/A            \t|\tN/A                  \t|\tN/A                \t|\t31.5                  \t|\t1920 x 1080\t|\tBlack        \t|\tN/A      \t|\tN/A  \t|\t130  \t|\t20";
    Assertions.assertEquals(
            rawExpectedOutput
            , result);
  }

  @Test
  void getItemWithIndexRetrievesCorrectItemData() throws IOException {
    String itemID = "97222f34-2e84-48a3-a165-97962cdc8c95";
    int itemFieldIndex = 0;
    final String result = Inventory.getItemField(itemID, itemFieldIndex);

    Assertions.assertEquals(
            "Monitor", result);
  }

  @Test
  void getItemWithFinalIndexRetrievesCorrectItemData() throws IOException {
    String itemID = "6162c2c6-f1f2-4402-b216-916483a34ee1";
    int itemFieldIndex = 13;

    final String result = Inventory.getItemField(itemID, itemFieldIndex);

    String expectedItemFieldValue = "15";
    Assertions.assertEquals(
            expectedItemFieldValue, result);
  }


  @Test
  void getItemForUnavailableItemReturnsCustomString() throws IOException {
    String itemID = "97222f34-2e84-48a3-a165-97962mmm8c95";
    final String result = Inventory.getItemRawData(itemID);

    Assertions.assertEquals(NOT_AVAILABLE_ITEM_TEXT
            , result);
  }
}